<?php

namespace Tests;

use PHPUnit\Framework\TestCase;
use App\Calculator;
use App\Cart;
use App\CartItem;
use App\Pan;
use App\Phone;
use App\TV;

class CartTest extends TestCase
{
    public function testUniqueness()
    {
        $firstCall = Calculator::getInstance();
        $secondCall = Calculator::getInstance();

        $this->assertInstanceOf(Calculator::class, $firstCall);
        $this->assertSame($firstCall, $secondCall);
    }

    /**
     * @dataProvider provider
     */
    public function testCalculator(array $items, $cost, $expected)
    {
        $cart = new Cart;
        /** @var CartItem $item */
        foreach ( $items as $item )
            $cart->add($item);

        $this->assertEquals(Calculator::getInstance()->getCost($cart) == $cost, $expected);
    }

    public function provider(): array
    {
        $tv = new TV;
        $tv->setPrice(50000);
        $phone = new Phone;
        $phone->setPrice(10000);
        $pan = new Pan;
        $pan->setPrice(2000);

        $items = [
            new CartItem($tv, 10),
            new CartItem($phone, 2),
            new CartItem($pan, 3),
        ];

        $cost = 0;
        /** @var CartItem $item */
        foreach ( $items as $item )
            $cost += $item->getCost();

        return [
            [ $items, $cost, true ],
            [ $items, ($cost * (-1)), false ]
        ];
    }
}
