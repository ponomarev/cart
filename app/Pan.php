<?php

namespace App;

final class Pan extends Product
{
    private $brand = 'Neva';

    public function __construct()
    {
        $characteristic = new Characteristic;
        $characteristic->name = 'Диаметр';
        $characteristic->value = 5;

        $this->characteristics[] = $characteristic;
    }
}
