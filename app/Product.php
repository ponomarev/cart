<?php

namespace App;

class Product
{
    private $brand = '';
    private $price = 0;
    protected $characteristics = [];

    public function getBrand(): string
    {
        return $this->brand;
    }

    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getCharacteristics(): array
    {
        return $this->characteristics;
    }
}