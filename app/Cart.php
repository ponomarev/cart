<?php

namespace App;

final class Cart
{
    private $items = [];

    public function add(CartItem $item): void
    {
        $this->items[] = $item;
    }

    public function getItems(): array
    {
        return $this->items;
    }

    public function delete(int $index)
    {
        if ( ! isset($this->items[ $index ]) )
            throw new \Exception(sprintf('Товар с индексом %d не найден', $index));

        unset($this->items[ $index ]);
    }
}
