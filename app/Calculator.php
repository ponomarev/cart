<?php

namespace App;

final class Calculator
{
    private static $instance = null;

    public static function getInstance(): Calculator
    {
        if ( static::$instance === null ) {
            static::$instance = new static();
        }

        return static::$instance;
    }

    private function __construct()
    {
    }

    private function __clone()
    {
    }

    public function __wakeup()
    {
    }

    public function getCost(Cart $cart): float
    {
        $cost = 0;

        $items = $cart->getItems();

        /** @var CartItem $item */
        foreach ( $items as $item ) {
            $cost += $item->getCost();
        }

        return $cost;
    }
}
