<?php

namespace App;

final class TV extends Product
{
    private $brand = 'Samsung';

    public function __construct()
    {
        $characteristic = new Characteristic;
        $characteristic->name = 'Диагональ';
        $characteristic->value = 40;

        $this->characteristics[] = $characteristic;
    }
}
