<?php

namespace App;

final class Phone extends Product
{
    private $brand = 'Xiaomi';

    public function __construct()
    {
        $characteristic = new Characteristic;
        $characteristic->name = 'Диагональ';
        $characteristic->value = 10;

        $this->characteristics[] = $characteristic;
    }
}
