<?php

namespace App;

class CartItem
{
    private $product;
    private $count;

    public function __construct(Product $product, int $count)
    {
        if($product->getPrice() <= 0) throw new \Exception('Цена должна быть больше 0');
        
        $this->product = $product;
        $this->count = $count;
    }

    public function getCost()
    {
        return $this->product->getPrice() * $this->count;
    }
}